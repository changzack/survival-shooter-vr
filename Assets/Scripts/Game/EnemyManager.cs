﻿using Managers;
using Players;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    #region Serialized Fields

    public GameObject enemy;
    public float spawnTime = 3f;
    public Transform[] spawnPoints;

    #endregion

    #region Non-serialized Fields

    private Player m_targetPlayer;

    #endregion

    #region Unity Event Functions

    private void Start()
    {
        InvokeRepeating(nameof(Spawn), spawnTime, spawnTime);
        m_targetPlayer = PlayerManager.Instance.GetRandomPlayer();
    }

    #endregion

    #region Private Methods

    private void Spawn()
    {
        if (m_targetPlayer.PlayerHealth.currentHealth <= 0f)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation, transform);
    }

    #endregion
}