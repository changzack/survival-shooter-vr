﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class GameUI : MonoBehaviour
{
    #region Static Fields and Constants

    private static readonly int GameOver = Animator.StringToHash("GameOver");

    #endregion

    #region Serialized Fields

    [SerializeField] private Image m_damageImage;
    [SerializeField] private Button m_restartButton;
    [SerializeField] private Button m_quitButton;
    [SerializeField] private XRRayInteractor m_uiInteractor;

    #endregion

    #region Non-serialized Fields

    private Animator m_animator;

    #endregion

    #region Properties, Indexers

    public Image DamageImage => m_damageImage;

    #endregion

    #region Unity Event Functions

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_quitButton.gameObject.SetActive(false);
        m_restartButton.gameObject.SetActive(false);
        m_uiInteractor.gameObject.SetActive(false);
    }

    #endregion

    #region Public APIs

    public void Die()
    {
        m_animator.SetTrigger(GameOver);
        m_quitButton.gameObject.SetActive(true);
        m_restartButton.gameObject.SetActive(true);
        m_uiInteractor.gameObject.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit()
    {
        Application.Quit();
    }

    #endregion
}