﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ContinuousMovement : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField] private XRRig xrRig;
    [SerializeField] private CharacterController character;
    [SerializeField] private float continuousMoveSpeed = 3.0f;
    [SerializeField] private float gravity = -9.81f;
    [SerializeField] private float additionalHeight;
    [SerializeField] private XRController m_xrController;

    #endregion

    #region Non-serialized Fields

    private float m_fallingSpeed;

    #endregion

    #region Unity Event Functions

    private void FixedUpdate()
    {
        Movement(m_xrController);
        CapsuleFollowHeadset();
    }

    #endregion

    #region Private Methods

    private void CapsuleFollowHeadset()
    {
        character.height = xrRig.cameraInRigSpaceHeight + additionalHeight;
        Vector3 capsuleCenter =
            character.transform.InverseTransformPoint(xrRig.cameraGameObject.transform.position);
        character.center = new Vector3(capsuleCenter.x, character.height * 0.5f + character.skinWidth,
            capsuleCenter.z);
    }

    private bool CheckIsGrounded()
    {
        Vector3 rayStart = character.transform.TransformPoint(character.center);
        float rayLength = character.center.y + 0.01f;
        bool isHit = Physics.SphereCast(rayStart, character.radius, Vector3.down, out RaycastHit hitInfo,
            rayLength);
        return isHit;
    }

    private void Movement(XRController controller)
    {
        if (CheckIsGrounded())
        {
            m_fallingSpeed = 0f;
        }
        else
        {
            m_fallingSpeed += gravity;
        }

        controller.inputDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 value);
        Quaternion headYaw = Quaternion.Euler(0, xrRig.cameraGameObject.transform.eulerAngles.y, 0);
        Vector3 direction = headYaw *
                            new Vector3(value.x * continuousMoveSpeed, m_fallingSpeed,
                                value.y * continuousMoveSpeed);

        character.Move(Time.fixedDeltaTime * direction);
    }

    #endregion
}