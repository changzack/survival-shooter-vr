﻿using System.Collections.Generic;
using Players;
using UnityEngine;

namespace Managers
{
    public class PlayerManager : MonoBehaviour
    {
        public static PlayerManager Instance { get; private set; }
        

        List<Player> players = new List<Player>();
    
        void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }

        public Player GetRandomPlayer()
        {
            int index = Random.Range(0, players.Count);
            return players[index];
        }

        public void RegisterPlayer(Player player)
        {
            players.Add(player);
        }
    }
}
