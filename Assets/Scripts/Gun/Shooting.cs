﻿using System;
using UnityEngine;

namespace Gun
{
    public class Shooting : MonoBehaviour
    {
        public bool IsActivated { get; set; }
        
        [SerializeField] GameObject gunBarrel;
        [SerializeField] int damagePerShot = 20;
        [SerializeField] float timeBetweenBullets = 0.15f;
        [SerializeField] float shootRange = 15f;

        float timer;
        Ray shootRay = new Ray();
        LineRenderer shootLine;
        ParticleSystem shootParticles;
        AudioSource shootAudio;
        Light shootLight;
        int shootableMask;
        float effectsDisplayTime = 0.2f;

        void Awake()
        {
            shootableMask = LayerMask.GetMask("Shootable");
            shootParticles = gunBarrel.GetComponent<ParticleSystem>();
            shootLine = gunBarrel.GetComponent<LineRenderer>();
            shootAudio = gunBarrel.GetComponent<AudioSource>();
            shootLight = gunBarrel.GetComponent<Light>();
        }

        void Update()
        {
            timer += Time.deltaTime;
            
            if(IsActivated && timer >= timeBetweenBullets && Math.Abs(Time.timeScale) > float.Epsilon)
                Shoot();

            if (timer >= timeBetweenBullets * effectsDisplayTime)
                DisableEffects();
        }

        void DisableEffects()
        {
            shootLight.enabled = false;
            shootLine.enabled = false;
        }

        void Shoot()
        {
            timer = 0;
            shootParticles.Stop();
            shootParticles.Play();
            
            shootLight.enabled = true;
            
            shootLine.enabled = true;
            var originPos = gunBarrel.transform.position;
            shootLine.SetPosition(0, originPos);
            
            shootAudio.Play();
            
            shootRay.origin = originPos;
            shootRay.direction = gunBarrel.transform.forward;

            if (Physics.Raycast(shootRay, out RaycastHit hitInfo, shootRange, shootableMask))
            {
                EnemyHealth enemyHealth = hitInfo.collider.GetComponent<EnemyHealth>();
                enemyHealth?.TakeDamage(damagePerShot, hitInfo.point);
                shootLine.SetPosition(1, hitInfo.point);
            }
            else
            {
                shootLine.SetPosition(1, gunBarrel.transform.position + gunBarrel.transform.forward * shootRange);
            }
        }
    }
}