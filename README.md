﻿This is a VR rework off of Unity Learn's Survival Shooter tutorial.

**Controls**
1. Grab: Right controller Grip Button to grab gun
2. Shoot: Right controller Trigger Button
3. Steering: Left controller Joystick
4. Teleport: Left controller Trigger Button
5. Snap Turn: Right controller Joystick   
6. UI: Right controller Trigger Button when game over

