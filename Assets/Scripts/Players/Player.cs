﻿using Managers;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Players
{
    /// <summary>
    ///     This scripts execution order is higher than the rest due to registration to PlayerManager
    /// </summary>
    public class Player : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private GameUI m_gameUI;

        #endregion

        #region Properties, Indexers

        public PlayerHealth PlayerHealth { get; private set; }
        public Collider PlayerCollider { get; private set; }

        #endregion

        #region Unity Event Functions

        private void Awake()
        {
            // Get Player related components
            // m_gameUI = Instantiate(m_gameUIPrefab, GetComponent<XRRig>().cameraGameObject.transform);
            PlayerHealth = GetComponent<PlayerHealth>();
            PlayerCollider = GetComponent<CharacterController>();

            // Initialize component
            PlayerHealth.Initialize(this, m_gameUI.DamageImage);

            // Register to manager
            PlayerManager.Instance.RegisterPlayer(this);
        }

        #endregion

        public void Die()
        {
            m_gameUI.Die();
        }
    }
}