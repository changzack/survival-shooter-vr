﻿using UnityEngine;
using UnityEngine.UI;

namespace Players
{
    public class PlayerHealth : MonoBehaviour
    {
        #region Serialized Fields

        public int currentHealth;
        [SerializeField] private int m_startingHealth = 100;
        [SerializeField] private AudioClip m_deathClip;
        [SerializeField] private float m_flashSpeed = 5f;
        [SerializeField] private Color m_flashColour = new Color(1f, 0f, 0f, 0.5f);

        #endregion

        #region Non-serialized Fields

        private bool damaged;
        private bool isDead;
        private Image m_damageImage;

        private AudioSource playerAudio;
        private Player m_player;

        #endregion

        #region Unity Event Functions

        private void Awake()
        {
            playerAudio = GetComponent<AudioSource>();
            currentHealth = m_startingHealth;
        }

        private void Update()
        {
            if (damaged)
            {
                m_damageImage.color = m_flashColour;
            }
            else
            {
                m_damageImage.color = Color.Lerp(m_damageImage.color, Color.clear, m_flashSpeed * Time.deltaTime);
            }

            damaged = false;
        }

        #endregion

        #region Private Methods

        private void Die()
        {
            isDead = true;
            playerAudio.clip = m_deathClip;
            playerAudio.Play();
            m_player.Die();
        }

        #endregion

        #region Public APIs

        public void Initialize(Player player, Image damageImage)
        {
            m_player = player;
            m_damageImage = damageImage;
        }

        public void TakeDamage(int amount)
        {
            damaged = true;
            currentHealth -= amount;
            playerAudio.Play();

            if (currentHealth <= 0 && !isDead)
            {
                Die();
            }
        }

        #endregion
    }
}