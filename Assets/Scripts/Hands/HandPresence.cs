﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class HandPresence : MonoBehaviour
{
    [SerializeField] InputDeviceCharacteristics characteristics;
    [SerializeField] GameObject handModel;
    [SerializeField] Animator handAnimator;
    GameObject spawnedHandModel;
    XRController xrController;
    static readonly int GripIndex = Animator.StringToHash("Grip");
    static readonly int TriggerIndex = Animator.StringToHash("Trigger");

    void Start()
    {
        xrController = GetComponentInParent<XRController>();
        if(xrController == null)
            Debug.LogError($"Cannot find controller in parents of {gameObject}");
    }

    void Update()
    {
        UpdateHandAnimation();
    }

    void UpdateHandAnimation()
    {
        if (xrController.inputDevice.TryGetFeatureValue(CommonUsages.grip, out var gripValue) && gripValue > 0.1f)
        {
            handAnimator.SetFloat(GripIndex, gripValue);
        }
        else
        {
            handAnimator.SetFloat(GripIndex, 0);
        }

        if (xrController.inputDevice.TryGetFeatureValue(CommonUsages.trigger, out var triggerValue) && triggerValue > 0.1f)
        {
            handAnimator.SetFloat(TriggerIndex, triggerValue);
        }
        else
        {
            handAnimator.SetFloat(TriggerIndex, 0);
        }
    }
}
