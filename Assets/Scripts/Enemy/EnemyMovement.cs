﻿using UnityEngine;
using System.Collections;
using Managers;
using Players;

public class EnemyMovement : MonoBehaviour
{
    Player targetPlayer;
    Collider playerCollider;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;

    void Start ()
    {
        targetPlayer = PlayerManager.Instance.GetRandomPlayer();
        playerCollider = targetPlayer.PlayerCollider;
        playerHealth = targetPlayer.PlayerHealth;
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
    }


    void Update ()
    {
        if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            nav.SetDestination (playerCollider.bounds.center);
        }
        else
        {
            nav.enabled = false;
        }
    }
}
