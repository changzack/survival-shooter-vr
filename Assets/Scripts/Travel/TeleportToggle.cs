﻿using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

namespace XRZack.Travel
{
    public class TeleportToggle : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private XRController m_teleportController;
        [SerializeField] private InputHelpers.Button activationButton;
        [SerializeField] private float pressedThreshold = 0.01f;

        #endregion

        #region Unity Event Functions

        private void Awake()
        {
            if (m_teleportController == null)
            {
                Debug.LogError("Teleport Controller is null", this);
            }
        }

        private void Update()
        {
            bool isPressed = IsPressed();
            
            if (isPressed != m_teleportController.gameObject.activeSelf)
            {
                m_teleportController.gameObject.SetActive(isPressed);       
            }
        }

        #endregion

        #region Private Methods

        private bool IsPressed()
        {
            InputDevice device = m_teleportController.inputDevice;
            device.IsPressed(activationButton, out bool isPressed, pressedThreshold);
            return isPressed;
        }

        #endregion
    }
}